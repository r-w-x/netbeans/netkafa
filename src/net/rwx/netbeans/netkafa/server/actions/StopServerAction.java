/*
 * Copyright 2016 Arnaud Fonce <arnaud.fonce@r-w-x.net>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.rwx.netbeans.netkafa.server.actions;

import net.rwx.netbeans.netkafa.server.KarafInstanceImplementation;
import org.openide.nodes.Node;

/**
 *
 * @author Arnaud Fonce <arnaud.fonce@r-w-x.net>
 */
public class StopServerAction extends AbstractServerAction {

    @Override
    protected void performAction(Node[] nodes) {
        for (Node node : nodes) {
            KarafInstanceImplementation server = lookupServerInstance(node);
            if (server.isRunning()) {
                server.stop();
            }
        }
    }

    @Override
    protected boolean enable(Node[] nodes) {
        for (Node node : nodes) {
            KarafInstanceImplementation server = lookupServerInstance(node);
            if (server != null && server.isRunning()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String getName() {
        return "Stop server";
    }

}
