/*
 * Copyright 2016 Arnaud Fonce <arnaud.fonce@r-w-x.net>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.rwx.netbeans.netkafa;

import java.io.IOException;
import net.rwx.netbeans.netkafa.server.KarafInstanceImplementation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.event.ChangeListener;
import org.netbeans.api.server.ServerInstance;
import org.netbeans.spi.server.ServerInstanceFactory;
import org.netbeans.spi.server.ServerInstanceProvider;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;
import org.openide.util.lookup.ServiceProviders;

/**
 *
 * @author Arnaud Fonce <arnaud.fonce@r-w-x.net>
 */
@ServiceProviders({
    @ServiceProvider(service = ServerInstanceProvider.class, path = "Servers"),
    @ServiceProvider(service = ServerInstanceProvider.class,
            path = Constants.INSTANCE_PROVIDER_PATH)
})
public class KarafInstanceProvider implements ServerInstanceProvider {

    private final Map<String, KarafInstanceImplementation> servers = new HashMap<>();
    private final ChangeSupport changeSupport = new ChangeSupport(this);
    private FileObject karafConfigRoot;

    public KarafInstanceProvider() {
        try {
            FileObject serverConfigRoot = FileUtil.getConfigFile("Servers");
            karafConfigRoot = FileUtil.createFolder(serverConfigRoot, "Netkafa");
            for (FileObject child : karafConfigRoot.getChildren()) {
                addNewServer(child);
            }
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @Override
    public List<ServerInstance> getInstances() {
        List<ServerInstance> serverInstances = new ArrayList<>();
        for (KarafInstanceImplementation implementation : servers.values()) {
            serverInstances.add(
                    ServerInstanceFactory.createServerInstance(implementation)
            );
        }
        return serverInstances;
    }

    private void addNewServer(FileObject instanceFile) {
        String name = instanceFile.getName();
        servers.put(
                name,
                new KarafInstanceImplementation(
                        name,
                        (String) instanceFile.getAttribute(Constants.PROP_INSTANCE_PATH),
                        (Boolean) instanceFile.getAttribute(Constants.PROP_CLEAN_ON_START),
                        (Integer) instanceFile.getAttribute(Constants.PROP_DEBUGGER_PORT)
                )
        );
        changeSupport.fireChange();
    }

    public void addNewServer(Map<String, Object> properties) {
        try {
            String name = (String) properties.get(Constants.PROP_INSTANCE_NAME);
            FileObject instanceFile = karafConfigRoot.createData(name);
            copyFromMapToFile(properties, instanceFile, Constants.PROP_INSTANCE_PATH);
            copyFromMapToFile(properties, instanceFile, Constants.PROP_CLEAN_ON_START);
            copyFromMapToFile(properties, instanceFile, Constants.PROP_DEBUGGER_PORT);
            addNewServer(instanceFile);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public void removeServer(String name) {
        try {
            FileObject instanceFile = karafConfigRoot.getFileObject(name);
            if( instanceFile != null ) {
                instanceFile.delete();
            }
            
            servers.remove(name);
            changeSupport.fireChange();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    private void copyFromMapToFile(
            Map<String, Object> map, 
            FileObject fileObject, 
            String key) throws IOException {
        fileObject.setAttribute(key, map.get(key));
    }
    
    @Override
    public void addChangeListener(ChangeListener listener) {
        changeSupport.addChangeListener(listener);
    }

    @Override
    public void removeChangeListener(ChangeListener listener) {
        changeSupport.removeChangeListener(listener);
    }

    public void updateIcon() {
        changeSupport.fireChange();
    }
}
