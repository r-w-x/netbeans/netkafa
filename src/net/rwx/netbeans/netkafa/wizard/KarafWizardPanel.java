/*
 * Copyright 2016 Arnaud Fonce <arnaud.fonce@r-w-x.net>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.rwx.netbeans.netkafa.wizard;

import java.awt.Component;
import javax.swing.event.ChangeListener;
import net.rwx.netbeans.netkafa.Constants;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.ChangeSupport;
import org.openide.util.HelpCtx;

/**
 *
 * @author Arnaud Fonce <arnaud.fonce@r-w-x.net>
 */
public class KarafWizardPanel implements WizardDescriptor.ValidatingPanel<WizardDescriptor> {

    private final KarafWizardComponent component;
    
    public KarafWizardPanel() {
        component = new KarafWizardComponent();
        String[] wizardSteps = {component.getName()};
        component.putClientProperty(WizardDescriptor.PROP_CONTENT_DATA, wizardSteps);
    }

    
    @Override
    public void validate() throws WizardValidationException {
        String formError = component.getFormError();
        if (!formError.isEmpty()) {
            throw new WizardValidationException(null, formError, null);
        }
    }

    @Override
    public synchronized Component getComponent() {
        return component;
    }

    @Override
    public HelpCtx getHelp() {
        return HelpCtx.DEFAULT_HELP;
    }

    @Override
    public void readSettings(WizardDescriptor data) {
    }

    @Override
    public void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty(
                Constants.PROP_INSTANCE_NAME, component.getInstanceName());
        wizardDescriptor.putProperty(
                Constants.PROP_INSTANCE_PATH, component.getInstancePath());
        wizardDescriptor.putProperty(
                Constants.PROP_CLEAN_ON_START, component.isCleanOnStart());
        wizardDescriptor.putProperty(
                Constants.PROP_DEBUGGER_PORT, component.getDebuggerPort());
    }

    @Override
    public boolean isValid() {
        return true;
    }

    private final ChangeSupport changeSupport = new ChangeSupport(this);
    
    @Override
    public void addChangeListener(ChangeListener cl) {
        changeSupport.addChangeListener(cl);
    }

    @Override
    public void removeChangeListener(ChangeListener cl) {
        changeSupport.removeChangeListener(cl);
    }
}
